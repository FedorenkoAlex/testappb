package com.dizzer.testtaskappb.presenter;

import android.widget.ImageView;

public interface IMainPresenter {
    void onCreate(int intentCode);

    void onSaveLink(int status, String imageLink);

    void onSaveImage(String imageLink, ImageView src);

    void onUpdateLink(int status, long id);

    void onDeleteLink(long id);
}
