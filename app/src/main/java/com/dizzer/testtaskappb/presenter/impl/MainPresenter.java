package com.dizzer.testtaskappb.presenter.impl;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.Toast;

import com.dizzer.testtaskappb.R;
import com.dizzer.testtaskappb.constant.ConstantsForDB;
import com.dizzer.testtaskappb.constant.CustomConstants;
import com.dizzer.testtaskappb.presenter.IMainPresenter;
import com.dizzer.testtaskappb.service.DeleteService;
import com.dizzer.testtaskappb.view.MainActivity;
import com.dizzer.testtaskappb.view.contractView.MainView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainPresenter implements IMainPresenter {
    private MainView view;
    private Context mContext;

    private long imageTime = System.currentTimeMillis();

    public MainPresenter(MainView view, Context mContext) {
        this.view = view;
        this.mContext = mContext;
    }

    @Override
    public void onCreate(int intentCode) {
        if (intentCode == CustomConstants.LAUNCHER) {
            view.closeApp();
        } else {
            view.showImage();
        }
    }

    @Override
    public void onSaveLink(int status, String imageLink) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConstantsForDB.IMAGE_LINK, imageLink);
        contentValues.put(ConstantsForDB.IMAGE_STATUS, status);
        contentValues.put(ConstantsForDB.IMAGE_TIME, imageTime);
        mContext.getContentResolver().insert(ConstantsForDB.CONTENT_URI_IMAGES, contentValues);
        view.showMsg(mContext.getResources().getString(R.string.link_saved_main_activity));
    }

    @Override
    public void onSaveImage(String imageLink, ImageView src) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            String filename = Uri.parse(imageLink).getLastPathSegment();

            Bitmap bitmap = ((BitmapDrawable) src.getDrawable()).getBitmap();
            File path = Environment.getExternalStorageDirectory();
            path = new File(path.getAbsoluteFile() + "/" + "BIGDIG/test/B");
            path.mkdirs();
            File sdFile = new File(path, filename);
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(sdFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                fileOutputStream.close();

                if (sdFile.exists()) {
                    view.showMsg(mContext.getResources().getString(R.string.save_image_txt) + sdFile.toString());

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            view.showMsg(mContext.getResources().getString(R.string.sd_card_error));
    }

    @Override
    public void onUpdateLink(int status, long id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ConstantsForDB.IMAGE_STATUS, status);
        contentValues.put(ConstantsForDB.IMAGE_TIME, imageTime);
        mContext.getContentResolver().update(ConstantsForDB.CONTENT_URI_IMAGES, contentValues, ConstantsForDB.IMAGE_ID + " = " + id, null);
    }

    @Override
    public void onDeleteLink(long id) {
        Intent intent = new Intent(mContext, DeleteService.class);
        intent.putExtra(CustomConstants.INTENT_ID, id);
        mContext.startService(intent);
    }
}
