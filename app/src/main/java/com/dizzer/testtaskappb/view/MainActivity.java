package com.dizzer.testtaskappb.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.dizzer.testtaskappb.R;
import com.dizzer.testtaskappb.constant.ConstantsForDB;
import com.dizzer.testtaskappb.constant.CustomConstants;
import com.dizzer.testtaskappb.presenter.IMainPresenter;
import com.dizzer.testtaskappb.presenter.impl.MainPresenter;
import com.dizzer.testtaskappb.view.contractView.MainView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.imageView)
    ImageView imageView;

    private IMainPresenter presenter;

    private ProgressDialog progressDialog;
    private Intent incomeIntent;
    private int followedIntent;

    private String imageLink;
    private long id;
    private int statusForLink = CustomConstants.STATUS_UNKNOWN;
    private int oldStatusForLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this, getApplicationContext());
        grantUriPermission(ConstantsForDB.CONTENT_AUTHORITY, ConstantsForDB.CONTENT_URI_IMAGES, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        incomeIntent = getIntent();
        followedIntent = incomeIntent.getIntExtra(CustomConstants.INTENT_CODE, CustomConstants.LAUNCHER);
        imageLink = incomeIntent.getStringExtra(CustomConstants.INTENT_LINK);
        id = incomeIntent.getLongExtra(CustomConstants.INTENT_ID, 0);
        oldStatusForLink = incomeIntent.getIntExtra(CustomConstants.INTENT_STATUS, 0);

        presenter.onCreate(followedIntent);
    }

    @Override
    public void showImage() {
        switch (followedIntent) {
            case CustomConstants.TEST_FRAGMENT:
                Picasso.get().load(imageLink).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        statusForLink = CustomConstants.STATUS_DOWNLOADED;
                        presenter.onSaveLink(statusForLink, imageLink);
                    }

                    @Override
                    public void onError(Exception e) {
                        statusForLink = CustomConstants.STATUS_ERROR;
                        presenter.onSaveLink(statusForLink, imageLink);
                    }
                });
                break;
            case CustomConstants.HISTORY_FRAGMENT:
                Picasso.get().load(imageLink).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        statusForLink = CustomConstants.STATUS_DOWNLOADED;
                        if (oldStatusForLink != statusForLink) {
                            presenter.onUpdateLink(statusForLink, id);
                        } else {
                            checkPermissions();
                            presenter.onDeleteLink(id);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        statusForLink = CustomConstants.STATUS_ERROR;
                        presenter.onUpdateLink(statusForLink, id);
                    }
                });
                break;
        }
    }

    @Override
    public void closeApp() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle(getString(R.string.title_for_dialog));
        new CountDownTimer(11000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (!isFinishing()) {
                    progressDialog.setMessage(getString(R.string.main_activity_massage) + " " +
                            millisUntilFinished / 1000 + " " + getString(R.string.main_activity_seconds_text));
                    progressDialog.show();
                }
            }

            public void onFinish() {
                progressDialog.dismiss();
                finish();
            }
        }.start();
    }

    @Override
    public void showMsg(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
    }

    public void checkPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            presenter.onSaveImage(imageLink, imageView);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            presenter.onSaveImage(imageLink, imageView);
        }
    }
}
