package com.dizzer.testtaskappb.view.contractView;

public interface MainView {
    void showImage();

    void closeApp();

    void showMsg(String string);
}
