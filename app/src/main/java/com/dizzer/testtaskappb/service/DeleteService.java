package com.dizzer.testtaskappb.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.dizzer.testtaskappb.R;
import com.dizzer.testtaskappb.constant.ConstantsForDB;
import com.dizzer.testtaskappb.constant.CustomConstants;

import java.util.concurrent.TimeUnit;

public class DeleteService extends IntentService {

    private long id;
    private Handler handler;

    public DeleteService() {
        super("Delete Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        handler = new Handler(getMainLooper());
        try {
            TimeUnit.SECONDS.sleep(15);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        id = intent.getLongExtra(CustomConstants.INTENT_ID, 0);
        getContentResolver().delete(ConstantsForDB.CONTENT_URI_IMAGES, "_id = " + id, null);
        showMsg();
    }

    private void showMsg(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(DeleteService.this.getApplicationContext(), getString(R.string.image_delete_msg),Toast.LENGTH_LONG).show();
            }
        });
    }
}
